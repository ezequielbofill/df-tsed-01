import { uuid } from '../utils/Uuid';

export class Product {
    id: string;
    name: string;
    description: string;
    price: number;

    constructor() {
        this.id = uuid.generate();
    }
}
