import { ProductOrder } from './product-order.model';
import { uuid } from '../utils/uuid';

export enum OrderStatus {
    'Template',
    'InProgress',
    'Finished',
}

export class Order {
    id: string;
    shippingStartDatetime: Date;
    shippingEndDatetime: Date;
    location: string;
    status: OrderStatus;
    totalPrice: number;
    products: ProductOrder[];

    constructor() {
        this.id = uuid.generate();
        this.status = OrderStatus.Template;
        this.totalPrice = 0;
        this.products = [];
    }
}
