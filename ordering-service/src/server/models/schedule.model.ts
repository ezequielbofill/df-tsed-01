import { Order } from './order.model';
import { uuid } from '../utils/uuid';

export enum ScheduleStatus {
    'PotentialClient',
    'Active',
    'Paused',
    'Canceled',
}

export class Schedule {
    public id: string;
    public userId: string;
    public orderList: Order[];
    public lastOrderDate: Date;
    public orderFrecuencyDays: number;
    public status: ScheduleStatus;

    constructor(customerId: string) {
        this.id = uuid.generate();
        this.orderList = [new Order()];
        this.userId = customerId;
    }
}
