import { Product } from './product.model';
import { uuid } from '../utils/Uuid';

export class ProductOrder {
    id: string;
    productInfo: Product;
    amount: number;
    price: number;

    constructor() {
        this.id = uuid.generate();
    }
}
