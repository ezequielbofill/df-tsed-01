import { GlobalAcceptMimesMiddleware, ServerLoader, ServerSettings } from '@tsed/common';
import * as bodyParser from 'body-parser';
import * as helmet from 'helmet';
import '@tsed/ajv';
import { config } from './config/config';
const rootDir = __dirname;

@ServerSettings({
    rootDir,
    acceptMimes: ['application/json', 'text/plain'],
    httpPort: config.port || 60601,
    httpsPort: 60001,
    mount: {
        '/rest': [`${rootDir}/controllers/**/*.ts`],
    },
    exclude: ['**/*.spec.ts'],
})
export class Server extends ServerLoader {
    $beforeRoutesInit() {
        this.use(bodyParser.json())
            .use(helmet())
            .use(
                bodyParser.urlencoded({
                    extended: true,
                })
            );

        return null;
    }
}
