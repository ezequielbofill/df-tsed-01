import { Injectable, Service } from '@tsed/di';
import { DatastoreClient } from '../clients/database.client';
import { Datastore } from '@google-cloud/datastore';
import { Schedule } from '../models/schedule.model';
import { entity, Entity } from '@google-cloud/datastore/build/src/entity';

@Service()
export class OrderScheduleService {
    private datastoreClient: Datastore;

    constructor(private datastore: DatastoreClient) {
        this.datastoreClient = datastore.getDatastoreClient();
    }

    public createScheduleDraft(customerId: string): string {
        const scheduleOrder: Schedule = new Schedule(customerId);

        const dsKey: entity.Key = this.datastoreClient.key(['Schedule']);

        const dsEntity: Entity = {
            key: dsKey,
            data: scheduleOrder,
        };
        console.log(dsKey);
        this.datastoreClient
            .save(dsEntity)
            .then(() => {
                console.log('saved');
            })
            .catch((err: any) => {
                console.log('error');
                console.log(err);
            });

        return scheduleOrder.id;
    }
}
