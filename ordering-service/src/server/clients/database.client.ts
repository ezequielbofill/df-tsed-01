import { config } from '../config/config';
import { Datastore } from '@google-cloud/datastore';

export const datastore: Datastore = new Datastore({
    projectId: config.applicationId,
});

export class DatastoreClient {
    private datastore: Datastore;
    constructor() {
        this.datastore = new Datastore({ projectId: config.applicationId });
    }

    getDatastoreClient() {
        return this.datastore;
    }
}

export const datastoreClient: Datastore = new DatastoreClient().getDatastoreClient();
