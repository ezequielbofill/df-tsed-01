import { merge } from 'lodash';

const configuration: any = {
    local: 'local',
    test: 'testing',
    prod: 'production',
    port: process.env.PORT || 60601,
    // 10 days in minutes
    expireTime: 24 * 60 * 10,
    secrets: {
        jwt: process.env.JWT || 'gumball',
    },
};

process.env.NODE_ENV = process.env.NODE_ENV || configuration.local;
configuration.env = process.env.NODE_ENV;

let envConfig: any;
try {
    // tslint:disable-next-line: no-var-requires
    envConfig = require('./' + configuration.env);
    envConfig = envConfig || {};
} catch (e) {
    envConfig = {};
}

export const config: any = merge(configuration, envConfig);
