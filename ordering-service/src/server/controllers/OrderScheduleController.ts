import { Controller, Post, PathParams } from '@tsed/common';
import { BadRequest } from '@tsed/exceptions';
import { OrderScheduleService } from '../services/OrderScheduleService';

@Controller('/schedule')
export class OrderScheduleController {
    constructor(private orderService: OrderScheduleService) {}

    @Post('/:customerId/draft')
    createScheduleDraft(@PathParams('customerId') customerId: string) {
        if (!!customerId && customerId !== 'undefined' && customerId !== 'null') {
            const scheduleId: string = this.orderService.createScheduleDraft(customerId);
            console.log(scheduleId);

            return { scheduleId };
        }

        throw new BadRequest('Invalid customerId.');
    }
}
