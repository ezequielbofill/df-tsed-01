import { expect } from 'chai';
import { TestContext } from '@tsed/testing';
import { OrderScheduleService } from '../../app/services/OrderScheduleService';

describe('OrderScheduleService', () => {
    beforeEach(TestContext.create);
    afterEach(TestContext.reset);

    it(
        'should do something',
        TestContext.inject([OrderScheduleService], (controller: OrderScheduleService) => {
            expect(controller).to.be.instanceof(OrderScheduleService);
        })
    );
});
