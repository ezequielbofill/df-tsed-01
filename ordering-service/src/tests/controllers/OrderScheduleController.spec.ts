import { expect } from 'chai';
import { TestContext } from '@tsed/testing';
import { OrderScheduleController } from '../../app/controllers/OrderScheduleController';

describe('OrderScheduleController', () => {
    beforeEach(TestContext.create);
    afterEach(TestContext.reset);

    it(
        'should do something',
        TestContext.inject([OrderScheduleController], (controller: OrderScheduleController) => {
            expect(controller).to.be.instanceof(OrderScheduleController);
        })
    );
});
