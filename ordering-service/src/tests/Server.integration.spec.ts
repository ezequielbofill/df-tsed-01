import { PlatformApplication } from '@tsed/common';
import { TestContext } from '@tsed/testing';
import * as SuperTest from 'supertest';
import { Server } from '../app/app';
import * as Sinon from 'sinon';
import { OrderScheduleController } from '../app/controllers/OrderScheduleController';
import { OrderScheduleService } from '../app/services/OrderScheduleService';
import { expect } from 'chai';
import { DatastoreClient } from '../app/clients/database.client';

describe('Server', () => {
    let request: SuperTest.SuperTest<SuperTest.Test>;

    beforeEach(TestContext.bootstrap(Server));
    beforeEach(
        TestContext.inject([PlatformApplication], (app: PlatformApplication) => {
            request = SuperTest(app.raw);
        })
    );

    afterEach(TestContext.reset);

    it('should call GET /rest', async () => {
        await request.get('/rest').expect(404);
    });

    describe('OrderScheduleController', () => {
        beforeEach(() => {
            const orderScheduleServiceStub = {
                createScheduleDraft: Sinon.stub().resolves('123456'),
            };
            const datastoreStub = {
                getDatastoreClient: Sinon.stub().resolves({
                    key: Sinon.stub().resolves({ namespace: undefined, kind: 'Schedule', path: undefined }),
                    save: Sinon.stub().resolves(new Promise(( anyParam: any ) => ({})))
                }),
            };
            const controller = TestContext.invoke(
                OrderScheduleController,
                [
                    {
                        provide: OrderScheduleService,
                        use: orderScheduleServiceStub
                    },
                    {
                        provide: DatastoreClient,
                        use: datastoreStub
                    }
                ]
            );
        });

        it('should call POST without body /rest/schedule/undefined/draft fail', async () => {
            await request.post('/rest/schedule/undefined/draft').expect(400);
        });

        it('should call POST without body /rest/schedule/null/draft fail', async () => {
            await request.post('/rest/schedule/null/draft').expect(400);
        });

        it('should call POST with body /rest/schedule/abc/draft success', async () => {
            const response = await request
                    .post('/rest/schedule/abc/draft')
                    .expect(200);
            expect(response.body).to.be.an('object');
            expect(response.body.scheduleId).to.be.eq('123456');
        });
    });
});
