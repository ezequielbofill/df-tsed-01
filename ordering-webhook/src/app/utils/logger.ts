import bunyan from 'bunyan';
import { LoggingBunyan } from '@google-cloud/logging-bunyan';
import { config } from '../config/config';

const loggingBunyan: LoggingBunyan = new LoggingBunyan({
    projectId: 'your-project-id',
    keyFilename: '/path/to/keyfile.json',
});

export const logger: bunyan = bunyan.createLogger({
    name: 'my-service',
    streams: [{ stream: process.stdout, level: 'info' }],
});

if (config.logging) {
    logger.addStream(loggingBunyan.stream('info'));
}
