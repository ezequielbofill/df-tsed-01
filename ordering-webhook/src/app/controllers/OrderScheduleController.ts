import {Controller, Get, Post, Injectable, BodyParams, Required} from '@tsed/common';
import { Request, Response } from 'express';
import {OrderServiceClient} from '../clients/order-service.client';

export interface ScheduleDraft {
  scheduleId: string;
}

@Controller('/schedule')
export class OrderScheduleController {

  constructor(private orderService: OrderServiceClient) {}

  @Post('/create-draft')
  async createScheduler(@Required() @BodyParams('customerId') customerId: string) {
      const scheduleId: string = await this.orderService.createScheduleDraft(customerId);

      return { scheduleId };
  }
}
