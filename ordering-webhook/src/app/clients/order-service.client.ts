import { config } from '../config/config';
// import * as unirest from 'unirest';
// import unirest from 'unirest';
import fetch from 'node-fetch';
export class OrderServiceClient {
    constructor() {}

    async createScheduleDraft(customerId: string): Promise<any> {
        const url: string = `${config.baseOrderingUrl}/rest/schedule/${customerId}/draft`;

        return fetch(url, { method: 'POST' })
            .then((res: any) => {
                return res.json();
            })
            .then((jsonRes: any) => {
                return jsonRes.scheduleIdeId;
            })
            .catch((err) => {
                throw new Error(err);
            });
    }
}
