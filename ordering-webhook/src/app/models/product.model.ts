import { uuid } from '../utils/uuid';

export class Product {
    id: string;
    name: string;
    description: string;
    price: number;

    constructor() {
        this.id = uuid.generate();
    }
}
