import { GlobalAcceptMimesMiddleware, ServerLoader, ServerSettings } from '@tsed/common';
import * as bodyParser from 'body-parser';
import * as compress from 'compression';
import * as cookieParser from 'cookie-parser';
import * as methodOverride from 'method-override';
import * as cors from 'cors';
import * as helmet from 'helmet';
import '@tsed/ajv';
import { config } from './config/config';
const rootDir = __dirname;

@ServerSettings({
    rootDir,
    acceptMimes: ['application/json', 'text/plain'],
    httpPort: config.port || 60600,
    httpsPort: 60000,
    mount: {
        '/rest': [`${rootDir}/controllers/**/*.ts`],
    },
    exclude: ['**/*.spec.ts'],
})
export class Server extends ServerLoader {
    $beforeRoutesInit() {
        this.use(bodyParser.json())
            .use(helmet())
            .use(
                bodyParser.urlencoded({
                    extended: true,
                })
            );

        return null;
    }
}
