module.exports = {
    applicationId: 'df-ordering-tst',
    credentialsPath: '',
    appName: '',
    logging: false,
    port: 60600,
    baseOrderingUrl: 'http://localhost:60601',
};
