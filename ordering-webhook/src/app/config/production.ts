module.exports = {
    applicationId: 'df-ordering-prod',
    credentialsPath: '',
    appName: '',
    logging: true,
    port: 60600,
    baseOrderingUrl: 'http://localhost:60601',
};
