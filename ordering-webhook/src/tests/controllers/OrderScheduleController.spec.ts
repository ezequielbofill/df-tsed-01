import { expect } from 'chai';
import { TestContext, bootstrap } from '@tsed/testing';
import { OrderScheduleController, ScheduleDraft } from '../../app/controllers/OrderScheduleController';
import * as Sinon from 'sinon';
import { OrderServiceClient } from '../../app/clients/order-service.client';

describe('OrderScheduleController', () => {
    before(TestContext.create);
    after(TestContext.reset);

    it(
        'should instantiate controller',
        TestContext.inject([OrderScheduleController], (controller: OrderScheduleController) => {
            expect(controller).to.be.instanceof(OrderScheduleController);
        })
    );
    it('should return a scheduleId', async () => {
        const orderServiceClientStub = {
            createScheduleDraft: Sinon.stub().resolves('123456'),
        };
        const orderScheduleCtrl = await TestContext.invoke(OrderScheduleController, [
            {
                provide: OrderServiceClient,
                use: orderServiceClientStub,
            },
        ]);
        const result = await orderScheduleCtrl.createScheduler('123');

        result.should.deep.equal({ scheduleId: '123456' });

        orderServiceClientStub.createScheduleDraft.should.be.calledWithExactly('123');

        orderScheduleCtrl.should.be.an.instanceof(OrderScheduleController);
        // orderScheduleCtrl.orderServiceClient.should.deep.equal(orderServiceClientStub);
    });
});
