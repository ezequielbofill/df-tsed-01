import { PlatformApplication } from '@tsed/common';
import { TestContext } from '@tsed/testing';
import * as SuperTest from 'supertest';
import { Server } from '../app/app';
import { expect } from 'chai';
import { OrderScheduleController } from '../app/controllers/OrderScheduleController';
import { OrderServiceClient } from '../app/clients/order-service.client';
import * as Sinon from 'sinon';

describe('Server', () => {
    let request: SuperTest.SuperTest<SuperTest.Test>;

    beforeEach(TestContext.bootstrap(Server));
    beforeEach(
        TestContext.inject([PlatformApplication], (app: PlatformApplication) => {
            request = SuperTest(app.raw);
        })
    );

    afterEach(TestContext.reset);

    it('should call GET /rest', async () => {
        await request.get('/rest').expect(404);
    });

    describe('OrderScheduleController', () => {
        beforeEach(() => {
            const orderServiceClientStub = {
                createScheduleDraft: Sinon.stub().resolves('123456'),
            };
            const controller = TestContext.invoke(OrderScheduleController, [{ provide: OrderServiceClient, use: orderServiceClientStub }]);
        });

        it('should call POST without body /rest/schedule/create-draft fail', async () => {
            await request.post('/rest/schedule/create-draft').expect(400);
        });

        it('should call POST with body /rest/schedule/create-draft success', async () => {
            const response = await request.post('/rest/schedule/create-draft').send({ customerId: '123' }).expect(200);
            expect(response.body).to.be.an('object');
            expect(response.body.scheduleId).to.be.an('string');
            expect(response.body.scheduleId).to.be.eq('123456');
        });
    });
});
